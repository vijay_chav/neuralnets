w_dict = {}
index_dict = {}
word_vectors = {}
with open("vectors.txt","r") as ins:
    lines = [line.split() for line in ins]

lines = lines[1:]

for iter in range(len(lines)):
    index_dict[lines[iter][0]] = iter+1
    lines[iter].insert(1,iter+1)
    w_dict[lines[iter][0]] = lines[iter][1:]
    word_vectors[lines[iter][0]] = lines[iter][2:]

#lines[id][0] gives word

print len(lines)
print len(lines[0])

ins.close()

import numpy

#
# output = {}
#
# dummy = numpy.zeros(output_size + 1)
#
# for i in range(0,len(lines)):
#     if lines[i][1] < output_size:
#         dummy[lines[i][1]] = 1
#         output[lines[i][0]] = dummy
#         dummy[lines[i][1]] = 0
#     else:
#         dummy[output_size] = 1
#         output[lines[i][0]] = dummy
#         dummy[output_size] = 0
#

#creating train_array and output array for train
with open("trainprocessed.txt","r") as ins:
    filelen = sum(1 for linelo in ins)
    print "Total no. of lines in train array %d" % (filelen/18 + 1)
ins.close()

n_train=5000
n_test = 1000
output_size = 3000

train_array = numpy.ndarray((n_train,18,180))
dummy = numpy.zeros(output_size + 1)
output_train = numpy.ndarray((n_train*18, output_size + 1))
x_train = numpy.ndarray((n_train,18))

with open("trainprocessed.txt","r") as ins:
    i=0
    iter=0
    j=0
    for line in ins:
        line = line[:-1]
        #print line
        if line in w_dict.keys():
            #print w_dict[line]
            train_array[iter,i] = w_dict[line][1:]
            index = w_dict[line][0]
            x_train[iter,i] = index

            if index >= output_size:
                index = output_size
            dummy[index] = 1
            output_train[j] = dummy
            dummy[index] = 0

            j +=1
            i += 1
            if i==18:
                i=0
                iter += 1
            if iter%100 ==0:
                print iter
            if iter==n_train:
                break

output_train[0:n_train*18-1] = output_train[1:]

print "done"
ins.close()


#cross checking errors
flag = 0
for iter in range(0,n_train):
    for i in range(0,18):
        if(train_array[iter][i][0] == 0):
            print iter
            print i
            flag = 1

if flag == 0:
    print "no errors in train_array"
else:
    print "error in train_array"


#creating test array and output array for test
with open("testprocessed.txt","r") as ins:
    filelen = sum(1 for lineli in ins)
    print "Total no. of lines in test array %d" % (filelen/18 + 1)
ins.close()

test_array = numpy.ndarray((n_test,18,180))
output_test = numpy.ndarray((n_test*18, output_size + 1))
x_test = numpy.ndarray((n_test,18))


with open("testprocessed.txt","r") as ins:
    i=0
    iter=0
    j=0
    for line in ins:
        line = line[:-1]
        #print line
        if line in w_dict.keys():
            #print w_dict[line]
            test_array[iter,i] = w_dict[line][1:]
            index = w_dict[line][0]
            x_test[iter,i] = index

            if index >= output_size:
                index = output_size
            dummy[index] = 1
            output_test[j] = dummy
            dummy[index] = 0

            j +=1
            i += 1
            if i==18:
                i=0
                iter += 1
            if iter%100 ==0:
                print iter
            if iter==n_test:
                break

output_test[0:n_test*18-1] = output_test[1:]

print "done"
ins.close()


#cross checking errors
flag = 0
for iter in range(0,n_test):
    for i in range(0,18):
        if(test_array[iter][i][0] == 0):
            print iter
            print i
            flag = 1

if flag == 0:
    print "no errors in test_array"
else:
    print "error in test_array"
